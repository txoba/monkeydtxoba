//Get the player's input
key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);

//React to inputs
move = key_left + key_right;
hsp = move * movespeed;
if(vsp < 10){
    vsp += grav;
}

//if(place_meeting(x,y+1,obj_background)){
//    vsp = key_jump * -jumpspeed;
//}

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}

//Horizontal Collision
if(place_meeting(x+hsp,y,obj_background)){
    while(!place_meeting(x+sign(hsp),y,obj_background)){
        x += sign(hsp);
        }
       hsp= 0;
}
x += hsp;
//Vertical Collision
if(place_meeting(x,y+vsp,obj_background)){
    while(!place_meeting(x,y+sign(vsp),obj_background)){
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;


if(global.pirates==1){ //1 luffy
//audio_play_sound(snd_overtaken, 10, true);
if(vsp!=0){
sprite_index = spr_luffy_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_luffy_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_luffy_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_luffy_jump;
image_speed = 0.5;
}
}else if(global.pirates==2){ //2 nicoRobin
//audio_play_sound(snd_nicoRobin_song, 10, true);
if(vsp!=0){
sprite_index = spr_nicoRobin_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_nicoRobin_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_nicoRobin_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_nicoRobin_jump;
image_speed = 0.5;
}
}else if(global.pirates==3){ //3 zoro
//audio_play_sound(snd_zoro_song, 10, true);
if(vsp!=0){
sprite_index = spr_zoro_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_zoro_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_zoro_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_zoro_jump;
image_speed = 0.5;
}
}else if(global.pirates==4){ //4 sanji
//audio_play_sound(snd_sanji_song, 10, true);
if(vsp!=0){
sprite_index = spr_sanji_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_sanji_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_sanji_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_sanji_jump;
image_speed = 0.5;
}
}else if(global.pirates==5){ //5 franky
//audio_play_sound(snd_franky_song, 10, true);
if(vsp!=0){
sprite_index = spr_franky_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_franky_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_franky_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_franky_jump;
image_speed = 0.5;
}
}
