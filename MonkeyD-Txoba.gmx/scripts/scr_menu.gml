luffy = keyboard_check(ord('1'));
nico = keyboard_check(ord('2'));
zoro = keyboard_check(ord('3'));
sanji = keyboard_check(ord('4'));
franky = keyboard_check(ord('5'));

if(luffy){
    audio_stop_all();
    global.pirates = 1;
    audio_play_sound(snd_overtaken, 10, true);
    room_goto(room_game1);
}
else if(nico){
    audio_stop_all();
    global.pirates = 2;
    audio_play_sound(snd_nicoRobin_song, 10, true);
    room_goto(room_game1);
}
else if(zoro){
    audio_stop_all();
    global.pirates = 3;
    audio_play_sound(snd_zoro_song, 10, true);
    room_goto(room_game1);
}
else if(sanji){
    audio_stop_all();
    global.pirates = 4;
    audio_play_sound(snd_sanji_song, 10, true);
    room_goto(room_game1);
}
else if(franky){
    audio_stop_all();
    global.pirates = 5;
    audio_play_sound(snd_franky_song, 10, true);
    room_goto(room_game1);
}
