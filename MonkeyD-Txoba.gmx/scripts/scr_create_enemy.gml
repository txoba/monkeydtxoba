///Initialize Variables
grav = 1.8;
hsp = 0;
vsp = 0;
jumpspeed = 20;
movespeed = 6;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;