//Get the player's input
key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);

//React to inputs
move = key_left + key_right;
hsp = move * movespeed;
if(vsp < 10){
    vsp += grav;
}
if(place_meeting(x,y+1,obj_background)){
    vsp = key_jump * -jumpspeed;
}


//Horizontal Collision
if(place_meeting(x+hsp,y,obj_background)){
    while(!place_meeting(x+sign(hsp),y,obj_background)){
        x += sign(hsp);
        }
       hsp= 0;
}
x += hsp;
//Vertical Collision
if(place_meeting(x,y+vsp,obj_background)){
    while(!place_meeting(x,y+sign(vsp),obj_background)){
        y += sign(vsp);
    }
    vsp = 0;
}
y += vsp;


if(global.pirates==1){ //1 luffy
//audio_play_sound(snd_overtaken, 10, true);
if(vsp!=0){
sprite_index = spr_luffy_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_luffy_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_luffy_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_luffy_jump;
image_speed = 0.5;
}
}else if(global.pirates==2){ //2 nicoRobin
//audio_play_sound(snd_nicoRobin_song, 10, true);
if(vsp!=0){
sprite_index = spr_nicoRobin_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_nicoRobin_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_nicoRobin_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_nicoRobin_jump;
image_speed = 0.5;
}
}else if(global.pirates==3){ //3 zoro
//audio_play_sound(snd_zoro_song, 10, true);
if(vsp!=0){
sprite_index = spr_zoro_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_zoro_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_zoro_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_zoro_jump;
image_speed = 0.5;
}
}else if(global.pirates==4){ //4 sanji
//audio_play_sound(snd_sanji_song, 10, true);
if(vsp!=0){
sprite_index = spr_sanji_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_sanji_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_sanji_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_sanji_jump;
image_speed = 0.5;
}
}else if(global.pirates==5){ //5 franky
//audio_play_sound(snd_franky_song, 10, true);
if(vsp!=0){
sprite_index = spr_franky_fall;
image_speed = 0.5;
}
else if(hsp==0 && vsp==0){
sprite_index = spr_franky_idle;
image_speed = 0.2;
}
else if(hsp !=0 && vsp==0){
sprite_index = spr_franky_walk;
image_speed = 0.2;
image_xscale=sign(hsp)*1;
}
else if(hsp==0 && vsp<0){
sprite_index = spr_franky_jump;
image_speed = 0.5;
}
}